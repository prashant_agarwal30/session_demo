<?php
include("config.php");
session_start();
if(!isset($_SESSION['firstname'])){
    header("location: login.php");
}

echo '<h3>Hi ' . $_SESSION['firstname'] . '</h3>';
$sql = "SELECT * FROM users";
$result = $conn -> query($sql);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>
<body>
    <div class="container mt-5">
        <div class="text-center">
            <h1 class="text-center d-inline ">users</h1>
            <a class="btn btn-danger" href="logout.php">Logout</a>
        </div>
        <table class=table>
            <head>
            <tr>
                <td>Id</td>
                <td>First Name</td>
                <td>Last Name</td>
                <td>Email</td>
                <td>Gender</td>
                <td>Action</td>
            </tr>
        
            </thead>
            <tbody>
                <?php
                    if($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                    
                ?>

                        <tr>
                            <td><?php echo $row['id']?></td>
                            <td><?php echo $row['firstname']?></td>                   
                            <td><?php echo $row['lastname']?></td>                    
                            <td><?php echo $row['email']?></td>
                            <td><?php echo $row['gender']?></td>
                            <?php if($_SESSION['firstname'] == 'prashant' || $row['firstname'] == $_SESSION['firstname']) { ?>
                            <td><a class="btn btn-info" href="update.php?id=<?php echo $row['id'] ?>">Update</a>
                                <a class="btn btn-warning" href="delete.php?id=<?php echo $row['id'] ?>">Delete</a></td>
                            
                            <?php } else { echo '<td></td>'; } ?>

                        </tr>

                <?php
                
                        }
                    }
                ?>

            </tbody>

        </table>
    </div>
</body>
</html>